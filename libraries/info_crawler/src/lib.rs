use camino::{Utf8Path, Utf8PathBuf};
use errors::VideoToolsError;
use info::Info;
use scan_config::versions::v1::VideoType;
use scan_config::ScanConfigHandler;
use std::ops::{Deref, DerefMut};

#[derive(Default, Debug)]
pub struct InfoCrawlerItem {
    info: Info,
    errors: Vec<VideoToolsError>,
    relative_folder: Utf8PathBuf,
}

impl InfoCrawlerItem {
    pub fn get_info(&mut self) -> &mut Info {
        &mut self.info
    }

    fn set_info(&mut self, info: Info) {
        self.info = info;
    }

    pub fn add_error(&mut self, error: VideoToolsError) {
        self.errors.push(error);
    }

    pub fn get_errors(&self) -> Vec<VideoToolsError> {
        self.errors.clone()
    }

    pub fn has_errors(&self) -> bool {
        !self.errors.is_empty()
    }

    pub fn get_relative_folder(&self) -> &str {
        self.relative_folder.as_str()
    }

    fn set_relative_folder(&mut self, relative_folder: Utf8PathBuf) {
        self.relative_folder = relative_folder
    }
}

#[derive(Default, Debug)]
pub struct InfoCrawler {
    data: Vec<InfoCrawlerItem>,
}

fn scan_item(path: &Utf8PathBuf, relative_folder: Utf8PathBuf) -> InfoCrawlerItem {
    let mut info_crawler_item = InfoCrawlerItem::default();
    info_crawler_item.set_relative_folder(relative_folder);

    match Info::new(path) {
        Ok(info) => info_crawler_item.set_info(info),
        Err(e) => info_crawler_item.add_error(e),
    }

    info_crawler_item
}

fn scan_folder(
    path: &Utf8PathBuf,
    relative_folder: Utf8PathBuf,
    tv_filter: bool,
) -> Vec<InfoCrawlerItem> {
    if path.is_file() {
        return Vec::new();
    }

    let dirs = match path.read_dir_utf8() {
        Ok(dirs) => dirs,
        _ => return Vec::new(),
    };

    let mut data = Vec::new();

    for dir in dirs.flatten() {
        if !dir.path().is_dir() {
            continue;
        }

        let mut folder = relative_folder.clone();

        let name = dir.file_name();
        if tv_filter && !name.to_lowercase().contains("season") {
            continue;
        }

        folder = folder.join(name);
        data.push(scan_item(&dir.into_path(), folder));
    }

    data
}

fn scan_for_tv(path: &Utf8PathBuf, relative_folder: Utf8PathBuf) -> Vec<InfoCrawlerItem> {
    if path.is_file() {
        return Vec::new();
    }

    let dirs = match path.read_dir_utf8() {
        Ok(dirs) => dirs,
        _ => return Vec::new(),
    };

    let mut data = Vec::new();

    for dir in dirs.flatten() {
        if !dir.path().is_dir() {
            continue;
        }

        let mut folder = relative_folder.clone();
        folder = folder.join(dir.file_name());

        data.extend(scan_folder(&dir.into_path(), folder, true));
    }

    data
}

impl InfoCrawler {
    pub fn get_info(
        &mut self,
        scan_config_handler: &ScanConfigHandler,
    ) -> Result<(), Vec<VideoToolsError>> {
        // Validate the scan config.
        scan_config_handler.validate()?;

        // Obtain the folders with types.
        let scan_config = scan_config_handler.get_scan_config();

        let scan_config_path = scan_config_handler.get_path();
        let scan_path = scan_config_path.parent().unwrap_or(Utf8Path::new(""));

        // Grab the Info list.
        for folder in scan_config.folders.clone().iter() {
            let path = scan_path.join(folder.relative_path.clone());

            if !path.exists() {
                continue;
            }

            if let Some(scan_type) = scan_config.types.get(&folder.type_id) {
                if scan_type.folder_type == VideoType::TV {
                    self.data
                        .append(&mut scan_for_tv(&path, folder.relative_path.clone()));
                } else {
                    self.data
                        .append(&mut scan_folder(&path, folder.relative_path.clone(), false));
                }
            }
        }

        Ok(())
    }
}

impl Deref for InfoCrawler {
    type Target = Vec<InfoCrawlerItem>;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for InfoCrawler {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}
