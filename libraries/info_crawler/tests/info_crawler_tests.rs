#[macro_use]
extern crate lazy_static;

#[cfg(test)]
use pretty_assertions::assert_eq;

use camino::Utf8PathBuf;
use errors::VideoToolsError;
use errors::VideoToolsError::{InfoError, ScanConfigError};
use info::Info;
use info_crawler::{InfoCrawler, InfoCrawlerItem};
use scan_config::ScanConfigHandler;
use std::env;

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files");
}

#[test]
fn test_default() {
    let info_crawler = InfoCrawler::default();
    assert_eq!(info_crawler.len(), 0);

    let mut info_crawler_item = InfoCrawlerItem::default();
    assert_eq!(info_crawler_item.get_info().clone(), Info::default());
    assert_eq!(info_crawler_item.get_errors(), Vec::new());
    assert_eq!(info_crawler_item.get_relative_folder(), Utf8PathBuf::new());
}

#[test]
fn test_invalid_scan_config() {
    let scan_config = ScanConfigHandler::default();
    let mut info_crawler = InfoCrawler::default();

    let result = info_crawler.get_info(&scan_config);
    assert_eq!(
        result.err().unwrap(),
        vec![ScanConfigError("Failed to find ``".to_string())]
    );
}

#[test]
fn test_valid() {
    let scan_config =
        ScanConfigHandler::new(FILES_DIR.join("valid").join("scan_config.json")).unwrap();
    let mut info_crawler = InfoCrawler::default();

    let result = info_crawler.get_info(&scan_config);
    assert_eq!(result.is_ok(), true);

    assert_eq!(info_crawler.len(), 2);

    assert_eq!(info_crawler[0].get_errors().len(), 0);
    let info_1 = info_crawler[0].get_info().clone();
    assert_eq!(info_1.get_iso_info(), Vec::new());
    assert_eq!(info_1.get_mkv_info(), Vec::new());
    assert_eq!(info_1.get_mkv_split_info(), Vec::new());
    assert_eq!(info_1.get_mkv_merge_info(), Vec::new());
    assert_eq!(info_1.get_mp4_data(), None);
    assert_eq!(info_1.get_mp4_split_info(), Vec::new());
    assert_eq!(info_1.get_mp4_merge_info(), Vec::new());

    assert_eq!(info_crawler[1].get_errors().len(), 0);
    let info_2 = info_crawler[0].get_info().clone();
    assert_eq!(info_2.get_iso_info(), Vec::new());
    assert_eq!(info_2.get_mkv_info(), Vec::new());
    assert_eq!(info_2.get_mkv_split_info(), Vec::new());
    assert_eq!(info_2.get_mkv_merge_info(), Vec::new());
    assert_eq!(info_2.get_mp4_data(), None);
    assert_eq!(info_2.get_mp4_split_info(), Vec::new());
    assert_eq!(info_2.get_mp4_merge_info(), Vec::new());
}

#[test]
fn test_invalid() {
    let invalid_folder = FILES_DIR.join("invalid");
    let scan_config = ScanConfigHandler::new(invalid_folder.join("scan_config.json")).unwrap();
    let mut info_crawler = InfoCrawler::default();

    let result = info_crawler.get_info(&scan_config);

    assert_eq!(result.is_ok(), true);

    fn generate_error(path: Utf8PathBuf) -> Vec<VideoToolsError> {
        let mut data = Vec::new();
        data.push(InfoError(format!("Failed to read `{}` with error: The system cannot find the file specified. (os error 2)", path)));
        data
    }

    assert_eq!(info_crawler.len(), 2);
    assert_eq!(
        info_crawler[0].get_errors(),
        generate_error(
            invalid_folder
                .join("Movies")
                .join("A Movie")
                .join("data.json")
        )
    );
    assert_eq!(
        info_crawler[1].get_errors(),
        generate_error(
            invalid_folder
                .join("TV Shows")
                .join("A TV Show")
                .join("Season 01")
                .join("data.json")
        )
    );
}
