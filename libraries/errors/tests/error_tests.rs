use errors::VideoToolsError::{InfoError, ScanConfigError, SettingsError};
#[cfg(test)]
use pretty_assertions::assert_eq;

#[test]
fn test_info_error() {
    let info_error = InfoError("test".into());
    assert_eq!(format!("{}", info_error), "[Info] test")
}

#[test]
fn test_scan_config_error() {
    let info_error = ScanConfigError("test".into());
    assert_eq!(format!("{}", info_error), "[Scan Config] test")
}

#[test]
fn test_settings_error() {
    let info_error = SettingsError("test".into());
    assert_eq!(format!("{}", info_error), "[Settings] test")
}
