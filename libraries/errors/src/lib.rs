use std::fmt::{Display, Formatter};

#[derive(Clone, Debug, PartialEq)]
pub enum VideoToolsError {
    InfoError(String),
    InfoCrawlerError(String),
    ScanConfigError(String),
    SettingsError(String),
    GeneralError(String),
}

impl Display for VideoToolsError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let (tag, data) = match self {
            VideoToolsError::InfoError(data) => ("[Info]", data),
            VideoToolsError::InfoCrawlerError(data) => ("[Info Crawler]", data),
            VideoToolsError::ScanConfigError(data) => ("[Scan Config]", data),
            VideoToolsError::SettingsError(data) => ("[Settings]", data),
            VideoToolsError::GeneralError(data) => ("[Video Tools]", data),
        };

        write!(f, "{} {}", tag, data)
    }
}
