#[macro_use]
extern crate lazy_static;

#[cfg(test)]
use pretty_assertions::{assert_eq, assert_ne};

use camino::Utf8PathBuf;
use errors::VideoToolsError::ScanConfigError;
use scan_config::versioned_scan_config::VersionedScanConfig;
use scan_config::versions::v1::{FolderType, ScanType, VideoType};
use scan_config::ScanConfigHandler;
use std::{env, fs};

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files");
}

fn remove_file_if_exists(path: Utf8PathBuf) {
    if path.exists() {
        fs::remove_file(path).unwrap()
    }
}

#[test]
fn test_default() {
    let mut scan_config_handler = ScanConfigHandler::new(FILES_DIR.join("default.json")).unwrap();
    assert_eq!(
        scan_config_handler.get_mut_scan_config(),
        ScanConfigHandler::default().get_mut_scan_config()
    )
}

#[test]
fn test_display() {
    let mut scan_config_handler = ScanConfigHandler::new(FILES_DIR.join("default.json")).unwrap();
    {
        let scan_config = scan_config_handler.get_mut_scan_config();
        scan_config.folders.push(FolderType {
            relative_path: Utf8PathBuf::from("Movies"),
            type_id: "movie".to_string(),
        });
        scan_config.types.insert(
            "movie".to_string(),
            ScanType {
                display_name: "Movies".to_string(),
                output_folder: "Movies".to_string(),
                folder_type: VideoType::MOVIE,
            },
        );
    }

    assert_eq!(
        format!("{}", scan_config_handler.get_scan_config()),
        "{\n  \"types\": {\n    \"movie\": {\n      \"display_name\": \"Movies\",\n      \"output_folder\": \"Movies\",\n      \"folder_type\": \"MOVIE\"\n    }\n  },\n  \"folders\": [\n    {\n      \"relative_path\": \"Movies\",\n      \"type\": \"movie\"\n    }\n  ]\n}")
}

#[test]
fn test_path() {
    let scan_config_handler = ScanConfigHandler::new(FILES_DIR.join("default.json")).unwrap();
    assert_eq!(
        scan_config_handler.get_path(),
        FILES_DIR.join("default.json")
    )
}

#[test]
fn test_save() {
    let path = FILES_DIR.join("save.json");
    let _cleanup = scopeguard::guard(path.clone(), remove_file_if_exists);

    let mut scan_config_handler = ScanConfigHandler::default();
    scan_config_handler.set_path(path.clone());
    {
        let scan_config = scan_config_handler.get_mut_scan_config();
        scan_config.types.insert(
            "movie".to_string(),
            ScanType {
                output_folder: "Movies".to_string(),
                display_name: "Movies".to_string(),
                folder_type: VideoType::MOVIE,
            },
        );
        scan_config.types.insert(
            "tv".to_string(),
            ScanType {
                output_folder: "TV Shows".to_string(),
                display_name: "TV Shows".to_string(),
                folder_type: VideoType::TV,
            },
        );

        scan_config.folders.push(FolderType {
            relative_path: Utf8PathBuf::from("Movies"),
            type_id: "movie".to_string(),
        });
        scan_config.folders.push(FolderType {
            relative_path: Utf8PathBuf::from("TV Shows"),
            type_id: "tv".to_string(),
        });
    }

    scan_config_handler.save().unwrap();

    let settings = scan_config_handler.get_scan_config();
    let scan_config_handler_load = ScanConfigHandler::new(path).unwrap();
    assert_eq!(settings, scan_config_handler_load.get_scan_config());
    assert_ne!(settings, VersionedScanConfig::default().get_scan_config());
}

#[test]
fn test_valid() {
    let scan_config_handler = ScanConfigHandler::new(
        FILES_DIR
            .join("valid")
            .join("config")
            .join("scan_config.json"),
    )
    .unwrap();
    {
        let scan_config = scan_config_handler.get_scan_config();
        let folders = scan_config.folders.clone();
        let types = scan_config.types.clone();

        {
            let folder_movies = folders[0].clone();
            assert_eq!(folder_movies.relative_path, "Movies");
            assert_eq!(folder_movies.type_id, "movie");
        }

        {
            let folder_tv_shows = folders[1].clone();
            assert_eq!(folder_tv_shows.relative_path, "TV Shows");
            assert_eq!(folder_tv_shows.type_id, "tv");
        }

        {
            let movie_type = types["movie"].clone();
            assert_eq!(movie_type.display_name, "Movies");
            assert_eq!(movie_type.output_folder, "Movies");
            assert_eq!(movie_type.folder_type, VideoType::MOVIE);
        }

        {
            let tv_type = types["tv"].clone();
            assert_eq!(tv_type.display_name, "TV Shows");
            assert_eq!(tv_type.output_folder, "TV Shows");
            assert_eq!(tv_type.folder_type, VideoType::TV);
        }
    }
}

#[test]
fn test_validate_invalid_path() {
    let scan_config_handler = ScanConfigHandler::default();
    assert_eq!(
        scan_config_handler.validate().err().unwrap(),
        vec![ScanConfigError("Failed to find ``".to_string())]
    )
}

#[test]
fn test_default_with_does_not_exist() {
    let exist_path = FILES_DIR.join("exist.json");
    let scan_config_handler = ScanConfigHandler::new(exist_path.clone()).unwrap();
    let mut default_scan_config_handler = ScanConfigHandler::default();
    default_scan_config_handler.set_path(exist_path);
    assert_eq!(scan_config_handler, default_scan_config_handler);
}

#[test]
fn test_validate_folders() {
    let scan_config_handler = ScanConfigHandler::new(
        FILES_DIR
            .join("valid")
            .join("config")
            .join("scan_config.json"),
    )
    .unwrap();
    assert_eq!(scan_config_handler.validate(), Ok(()))
}

#[test]
fn test_validate_output() {
    let scan_config_handler = ScanConfigHandler::new(
        FILES_DIR
            .join("valid")
            .join("config")
            .join("scan_config.json"),
    )
    .unwrap();
    assert_eq!(
        scan_config_handler.validate_output_folders(&FILES_DIR.join("valid").join("output")),
        Ok(())
    )
}

#[test]
fn test_missing_folders() {
    let path = FILES_DIR.join("missing_folders.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `folders`",
            path
        )))
    );
}

#[test]
fn test_missing_folders_relative_path() {
    let path = FILES_DIR.join("missing_folders_relative_path.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `relative_path`",
            path
        )))
    );
}

#[test]
fn test_invalid_folders_type() {
    let path = FILES_DIR.join("invalid_folders_type.json");
    let scan_config = ScanConfigHandler::new(path).unwrap();
    let errors = scan_config.validate().unwrap_err();
    assert_eq!(errors.len(), 2);
    assert_eq!(
        errors[0],
        ScanConfigError("Folders index `0`, failed to find key `m` in types".to_string())
    );
    assert_eq!(
        errors[1],
        ScanConfigError("Folders index `1`, failed to find key `m2` in types".to_string())
    );
}

#[test]
fn test_missing_folders_type() {
    let path = FILES_DIR.join("missing_folders_type.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `type`",
            path
        )))
    );
}

#[test]
fn test_missing_types() {
    let path = FILES_DIR.join("missing_types.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `types`",
            path
        )))
    );
}

#[test]
fn test_missing_types_display_name() {
    let path = FILES_DIR.join("missing_types_display_name.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `display_name`",
            path
        )))
    );
}

#[test]
fn test_missing_types_folder_type() {
    let path = FILES_DIR.join("missing_types_folder_type.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `folder_type`",
            path
        )))
    );
}

#[test]
fn test_missing_types_output_folder() {
    let path = FILES_DIR.join("missing_types_output_folder.json");
    assert_eq!(
        ScanConfigHandler::new(path.clone()).err(),
        Some(ScanConfigError(format!(
            "Failed to read scan_config `{}` with error: missing field `output_folder`",
            path
        )))
    );
}
