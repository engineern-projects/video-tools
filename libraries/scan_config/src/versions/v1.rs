use camino::Utf8PathBuf;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub enum VideoType {
    #[default]
    MOVIE,
    TV,
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct ScanType {
    pub display_name: String,
    pub output_folder: String,
    pub folder_type: VideoType,
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct FolderType {
    pub relative_path: Utf8PathBuf,

    #[serde(rename = "type")]
    pub type_id: String,
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct ScanConfig {
    pub types: HashMap<String, ScanType>,
    pub folders: Vec<FolderType>,
}

impl fmt::Display for ScanConfig {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            serde_json::to_string_pretty(&self).unwrap_or_default()
        )
    }
}
