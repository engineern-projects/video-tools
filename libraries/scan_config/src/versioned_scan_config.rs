use super::versions;
use camino::Utf8PathBuf;
use errors::VideoToolsError;
use errors::VideoToolsError::ScanConfigError;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(tag = "version")]
pub enum VersionedScanConfig {
    V1(versions::v1::ScanConfig),
}

impl Default for VersionedScanConfig {
    fn default() -> Self {
        VersionedScanConfig::V1(versions::v1::ScanConfig::default())
    }
}

impl VersionedScanConfig {
    pub fn get_mut_scan_config(&mut self) -> &mut versions::v1::ScanConfig {
        match self {
            VersionedScanConfig::V1(ref mut scan_config) => scan_config,
        }
    }

    pub fn get_scan_config(&self) -> &versions::v1::ScanConfig {
        match self {
            VersionedScanConfig::V1(ref scan_config) => scan_config,
        }
    }

    pub fn validate(
        &self,
        output_folder: &Utf8PathBuf,
        scan_folder: &Utf8PathBuf,
    ) -> Result<(), Vec<VideoToolsError>> {
        if output_folder == scan_folder {
            return Err(vec![ScanConfigError(
                "Output and Scan folder cannot be the same.".into(),
            )]);
        }

        let VersionedScanConfig::V1(config) = self;

        let mut errors = Vec::new();
        let mut output_folders: Vec<String> = Vec::new();
        let mut current_folders: Vec<Utf8PathBuf> = Vec::new();

        for type_data in config.types.clone() {
            let type_id = type_data.0;
            let type_folder = type_data.1.output_folder;
            let path = output_folder.join(&type_folder);
            if !(path.exists() && path.is_dir()) {
                errors.push(ScanConfigError(format!(
                    "Failed to find path '{}' for type '{}'",
                    path, type_folder
                )));
            }

            if output_folders.contains(&type_folder) {
                errors.push(ScanConfigError(format!(
                    "Found duplicate of output folder for type '{}'",
                    type_id
                )));
            } else {
                output_folders.push(type_folder);
            }
        }

        for folder_data in config.folders.clone() {
            if config.types.contains_key(folder_data.type_id.as_str()) {
                let path = scan_folder.join(&folder_data.relative_path);
                if !(path.exists() && path.is_dir()) {
                    errors.push(ScanConfigError(format!(
                        "Failed to find path '{}' for folder '{}'",
                        path, folder_data.relative_path
                    )));
                }

                if current_folders.contains(&folder_data.relative_path) {
                    errors.push(ScanConfigError(format!(
                        "Found duplicate of folder '{}'",
                        folder_data.relative_path
                    )));
                } else {
                    current_folders.push(folder_data.relative_path);
                }
            } else {
                errors.push(ScanConfigError(format!(
                    "Failed to find type '{}' for folder '{}'",
                    folder_data.type_id, folder_data.relative_path
                )));
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}
