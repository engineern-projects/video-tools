use crate::versioned_scan_config::VersionedScanConfig;
use camino::Utf8PathBuf;
use errors::VideoToolsError;
use errors::VideoToolsError::ScanConfigError;
use std::fs;

pub mod versioned_scan_config;
pub mod versions;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct ScanConfigHandler {
    scan_config: VersionedScanConfig,
    path: Utf8PathBuf,
}

impl ScanConfigHandler {
    pub fn new(path: Utf8PathBuf) -> Result<ScanConfigHandler, VideoToolsError> {
        if path.is_relative() {
            return Err(ScanConfigError(format!(
                "Given scan_config path is relative `{}`",
                path
            )));
        }

        match fs::read_to_string(&path) {
            Ok(s) => match serde_json::from_str::<VersionedScanConfig>(&s) {
                Ok(scan_config) => Ok(ScanConfigHandler { scan_config, path }),
                Err(e) => Err(ScanConfigError(format!(
                    "Failed to read scan_config `{}` with error: {}",
                    path, e
                ))),
            },
            Err(_e) => Ok(ScanConfigHandler {
                scan_config: VersionedScanConfig::default(),
                path,
            }),
        }
    }

    pub fn set_path(&mut self, path: Utf8PathBuf) {
        self.path = path;
    }

    pub fn get_path(&self) -> Utf8PathBuf {
        self.path.clone()
    }

    pub fn save(&self) -> Result<(), VideoToolsError> {
        let json_string = match serde_json::to_string_pretty(&self.scan_config) {
            Ok(s) => s,
            Err(e) => {
                return Err(ScanConfigError(format!(
                    "Failed to convert scan_config to String with error: {}",
                    e
                )))
            }
        };

        let folder_path = match self.path.parent() {
            Some(p) => p,
            None => {
                return Err(ScanConfigError(format!(
                    "Failed to obtain parent of path `{}`",
                    self.path
                )))
            }
        };

        if let Err(e) = fs::create_dir_all(folder_path) {
            return Err(ScanConfigError(format!(
                "Failed to create folder path `{}` with error: {}",
                folder_path, e
            )));
        }

        if let Err(e) = fs::write(&self.path, json_string) {
            return Err(ScanConfigError(format!(
                "Failed to save `{}` with error: {}",
                self.path, e
            )));
        }

        Ok(())
    }

    pub fn validate(&self) -> Result<(), Vec<VideoToolsError>> {
        if !self.path.exists() {
            return Err(vec![ScanConfigError(format!(
                "Failed to find `{}`",
                self.path
            ))]);
        }

        let scan_config = self.scan_config.get_scan_config();

        let mut errors = Vec::new();

        for (index, folder) in scan_config.folders.iter().enumerate() {
            if !scan_config.types.contains_key(folder.type_id.as_str()) {
                errors.push(ScanConfigError(format!(
                    "Folders index `{}`, failed to find key `{}` in types",
                    index, folder.type_id
                )))
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }

    pub fn validate_output_folders(
        &self,
        output_folder: &Utf8PathBuf,
    ) -> Result<(), Vec<VideoToolsError>> {
        let scan_path = match self.path.parent() {
            Some(p) => p.to_path_buf(),
            None => {
                return Err(vec![ScanConfigError(format!(
                    "Failed to obtain parent of path `{}`",
                    self.path
                ))])
            }
        };

        self.scan_config.validate(output_folder, &scan_path)
    }

    pub fn get_mut_scan_config(&mut self) -> &mut versions::v1::ScanConfig {
        self.scan_config.get_mut_scan_config()
    }

    pub fn get_scan_config(&self) -> &versions::v1::ScanConfig {
        self.scan_config.get_scan_config()
    }
}
