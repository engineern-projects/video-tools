#[macro_use]
extern crate lazy_static;

#[cfg(test)]
use pretty_assertions::assert_eq;

use camino::Utf8PathBuf;
use chrono::{DateTime, Utc};
use info::data::Resolution;
use info::Info;
use std::env;

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files");
}

#[test]
fn test_default() {
    let folder = FILES_DIR.join("default");

    let info = Info::new(&folder).unwrap();
    let info_default = Info::default();

    assert_eq!(info.get_iso_info(), info_default.get_iso_info());
    assert_eq!(info.get_mkv_merge_info(), info_default.get_mkv_merge_info());
    assert_eq!(info.get_mkv_split_info(), info_default.get_mkv_split_info());
    assert_eq!(info.get_mkv_info(), info_default.get_mkv_info());
    assert_eq!(info.get_mp4_merge_info(), info_default.get_mp4_merge_info());
    assert_eq!(info.get_mp4_split_info(), info_default.get_mp4_split_info());
    assert_eq!(info.get_mp4_data(), info_default.get_mp4_data());
}

#[test]
fn test_valid() {
    let folder = FILES_DIR.join("valid");
    let info = Info::new(&folder).unwrap();

    {
        // Iso information
        let iso_info = info.get_iso_info();
        assert_eq!(iso_info.len(), 1);

        // Data
        assert_eq!(iso_info[0].data.get_filename(), "movie.iso");
        assert_eq!(iso_info[0].data.get_check_only(), None);
        assert_eq!(iso_info[0].data.get_custom_mkv_folder(), None);

        // Status
        assert_eq!(iso_info[0].status.get_filename(), "movie.iso");
        let date: DateTime<Utc> = Default::default();
        assert_eq!(iso_info[0].status.last_checked, date);
        assert_eq!(iso_info[0].status.mkv_extracted, false);
        assert_eq!(iso_info[0].status.mkv_count, 0);
        assert_eq!(iso_info[0].status.hash, "");
    }

    {
        // MKV Split Information
        let mkv_split_info = info.get_mkv_split_info();
        assert_eq!(mkv_split_info.len(), 1);

        // Data Checks
        assert_eq!(mkv_split_info[0].data.get_filename(), "movie_t04.mkv");
        assert_eq!(
            mkv_split_info[0].data.get_source().unwrap_or_default(),
            "movie.iso"
        );
        assert_eq!(
            mkv_split_info[0].data.get_chapters().unwrap_or_default(),
            "1,2"
        );
        assert_eq!(
            mkv_split_info[0].data.get_timestamps().unwrap_or_default(),
            ""
        );

        // Status Checks
        assert_eq!(mkv_split_info[0].status.get_filename(), "movie_t04.mkv");
        assert_eq!(mkv_split_info[0].status.split, false);
    }

    {
        // MKV Merge Information
        let mkv_merge_info = info.get_mkv_merge_info();
        assert_eq!(mkv_merge_info.len(), 1);

        // Data Checks
        assert_eq!(mkv_merge_info[0].data.get_output_name(), "movie_merge.mkv");
        let filenames = mkv_merge_info[0].data.get_filenames();
        assert_eq!(filenames[0].get_filename(), "movie_t04-001.mkv");
        assert_eq!(filenames[0].get_source(), None);
        assert_eq!(filenames[1].get_filename(), "movie_t04-002.mkv");
        assert_eq!(filenames[1].get_source(), None);
        assert_eq!(filenames[2].get_filename(), "movie_t04-003.mkv");
        assert_eq!(filenames[2].get_source(), None);
        assert_eq!(filenames.len(), 3);

        // Status Checks
        assert_eq!(mkv_merge_info[0].status.get_filename(), "movie_merge.mkv");
        assert_eq!(mkv_merge_info[0].status.merged, false);
    }

    {
        // MKV Information
        let mkv_info = info.get_mkv_info();
        assert_eq!(mkv_info.len(), 1);

        // Data Checks
        assert_eq!(mkv_info[0].data.get_filename(), "movie_merge.mkv");
        assert_eq!(mkv_info[0].data.get_resolution(), Resolution::R2160p);
        assert_eq!(mkv_info[0].data.get_plex_name(), "movie (2000)");
        assert_eq!(mkv_info[0].data.get_source().unwrap_or_default(), "");

        // Status Checks
        assert_eq!(mkv_info[0].status.get_filename(), "movie_merge.mkv");
        assert_eq!(mkv_info[0].status.converted_original, false);
        assert_eq!(mkv_info[0].status.converted_tv, false);
        assert_eq!(mkv_info[0].status.converted_mobile, false);
        assert_eq!(mkv_info[0].status.extracted_subtitles, false);
    }

    {
        // Mp4 Split Information
        let mp4_split_info = info.get_mp4_split_info();
        assert_eq!(mp4_split_info.len(), 1);

        // Data Checks
        assert_eq!(mp4_split_info[0].data.get_filename(), "movie (2000).mp4");
        assert_eq!(mp4_split_info[0].data.get_source().unwrap_or_default(), "");
        assert_eq!(
            mp4_split_info[0].data.get_chapters().unwrap_or_default(),
            ""
        );
        assert_eq!(
            mp4_split_info[0].data.get_timestamps().unwrap_or_default(),
            "00:10:00,00:15:00"
        );

        // Status Checks
        assert_eq!(mp4_split_info[0].status.get_filename(), "movie (2000).mp4");
        assert_eq!(mp4_split_info[0].status.split, false);
    }

    {
        // Mp4 Merge Information
        let mp4_merge_info = info.get_mp4_merge_info();
        assert_eq!(mp4_merge_info.len(), 1);

        // Data Checks
        assert_eq!(
            mp4_merge_info[0].data.get_output_name(),
            "movie (2000) merge.mp4"
        );
        let filenames = mp4_merge_info[0].data.get_filenames();
        assert_eq!(filenames[0].get_filename(), "movie (2000)-001.mp4");
        assert_eq!(filenames[0].get_source(), None);
        assert_eq!(filenames[1].get_filename(), "movie (2000)-002.mp4");
        assert_eq!(filenames[1].get_source(), None);
        assert_eq!(filenames[2].get_filename(), "movie (2000)-003.mp4");
        assert_eq!(filenames[2].get_source(), None);
        assert_eq!(filenames.len(), 3);

        // Status Checks
        assert_eq!(
            mp4_merge_info[0].status.get_filename(),
            "movie (2000) merge.mp4"
        );
        assert_eq!(mp4_merge_info[0].status.merged, false);
    }

    {
        // Mp4 Data Checks
        let mp4_data = info.get_mp4_data().unwrap_or_default();

        // Check Items
        let items = mp4_data.get_items();
        assert_eq!(items.len(), 1);

        assert_eq!(items[0].get_pattern(), "movie (2000) merge");
        assert_eq!(items[0].get_exclusion().len(), 0);
        assert_eq!(items[0].get_type_id(), "movie");
        assert_eq!(items[0].get_season(), None);
        assert_eq!(items[0].get_folder_name(), "movie (2000)");

        // Check Corrections
        let corrections = mp4_data.get_corrections();
        assert_eq!(corrections.len(), 1);

        assert_eq!(
            corrections[0].get_pattern().unwrap_or_default(),
            "movie (2000) merge"
        );
        assert_eq!(corrections[0].get_replacement(), "movie (2000)");
        assert_eq!(corrections[0].get_regex(), None);
    }
}
