use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

mod custom_date_format {
    use chrono::{DateTime, TimeZone, Utc};
    use serde::{Deserialize, Deserializer, Serializer};

    const FORMAT: &str = "%m-%d-%Y %H:%M:%S";

    pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Utc.datetime_from_str(&s, FORMAT)
            .map_err(serde::de::Error::custom)
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct IsoStatus {
    filename: String,

    #[serde(with = "custom_date_format")]
    pub last_checked: DateTime<Utc>,
    pub mkv_extracted: bool,
    pub mkv_count: u32,

    #[serde(skip_deserializing)]
    #[serde(skip_serializing)]
    pub hash: String,
}

impl IsoStatus {
    pub fn new(filename: String) -> Self {
        let (last_checked, mkv_extracted, mkv_count, hash) = Default::default();
        Self {
            filename,
            last_checked,
            mkv_extracted,
            mkv_count,
            hash,
        }
    }

    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct MergeStatus {
    filename: String,
    pub merged: bool,
}

impl MergeStatus {
    pub fn new(filename: String) -> Self {
        let merged = Default::default();
        Self { filename, merged }
    }

    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct SplitStatus {
    filename: String,
    pub split: bool,
}

impl SplitStatus {
    pub fn new(filename: String) -> Self {
        let split = Default::default();
        Self { filename, split }
    }

    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct MkvStatus {
    filename: String,
    pub converted_original: bool,
    pub converted_tv: bool,
    pub converted_mobile: bool,
    pub extracted_subtitles: bool,
}

impl MkvStatus {
    pub fn new(filename: String) -> Self {
        let (converted_original, converted_tv, converted_mobile, extracted_subtitles) =
            Default::default();
        Self {
            filename,
            converted_original,
            converted_tv,
            converted_mobile,
            extracted_subtitles,
        }
    }

    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }
}

fn default_warning() -> String {
    "Warning! This file is auto generated, please do not touch.".to_string()
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Status {
    #[serde(rename = "__DO_NOT_TOUCH__")]
    #[serde(skip_deserializing)]
    #[serde(default = "default_warning")]
    notice: String,

    #[serde(default)]
    pub iso: Vec<IsoStatus>,

    #[serde(default)]
    pub mkv_split: Vec<SplitStatus>,

    #[serde(default)]
    pub mkv_merge: Vec<MergeStatus>,

    #[serde(default)]
    pub mkv: Vec<MkvStatus>,

    #[serde(default)]
    pub mp4_split: Vec<SplitStatus>,

    #[serde(default)]
    pub mp4_merge: Vec<MergeStatus>,
}
