use crate::data::{Data, IsoData, MergeData, MkvData, Mp4Data, SplitData};
use crate::status::{IsoStatus, MergeStatus, MkvStatus, SplitStatus, Status};
use camino::Utf8PathBuf;
use errors::VideoToolsError;
use errors::VideoToolsError::InfoError;
use std::fs;

pub mod data;
pub mod status;

#[derive(Debug, Clone, PartialEq)]
pub struct IsoInfo {
    pub data: IsoData,
    pub status: IsoStatus,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MkvInfo {
    pub data: MkvData,
    pub status: MkvStatus,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MergeInfo {
    pub data: MergeData,
    pub status: MergeStatus,
}

#[derive(Debug, Clone, PartialEq)]
pub struct SplitInfo {
    pub data: SplitData,
    pub status: SplitStatus,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct Info {
    data: Data,
    status: Status,

    folder: Utf8PathBuf,
}

impl Info {
    pub fn new(folder: &Utf8PathBuf) -> Result<Self, VideoToolsError> {
        // Read the data.json file into a `Data`.
        let data_path = folder.join("data.json");
        let data = match fs::read_to_string(data_path.clone()) {
            Ok(s) => match serde_json::from_str::<Data>(&s) {
                Ok(d) => d,
                Err(e) => {
                    return Err(InfoError(format!(
                        "Failed to read `{}` with error: {}",
                        data_path, e
                    )))
                }
            },
            Err(e) => {
                return Err(InfoError(format!(
                    "Failed to read `{}` with error: {}",
                    data_path, e
                )))
            }
        };

        // Read the status.json file into a `Status`.
        let status_path = folder.join("status.json");
        let mut info = if status_path.exists() {
            let status = match fs::read_to_string(status_path.clone()) {
                Ok(s) => match serde_json::from_str::<Status>(&s) {
                    Ok(d) => d,
                    Err(e) => {
                        return Err(InfoError(format!(
                            "Failed to read `{}` with error: {}",
                            status_path, e
                        )))
                    }
                },
                Err(e) => {
                    return Err(InfoError(format!(
                        "Failed to read `{}` with error: {}",
                        status_path, e
                    )))
                }
            };

            Self {
                data,
                status,
                folder: folder.clone(),
            }
        } else {
            Self {
                data,
                status: Default::default(),
                folder: folder.clone(),
            }
        };

        // Sync up the status of each type.
        let iso_info = info.get_iso_info();
        info.status.iso.clear();
        for iso in iso_info {
            info.status.iso.push(iso.status);
        }

        let mkv_merge_info = info.get_mkv_merge_info();
        info.status.mkv_merge.clear();
        for merge in mkv_merge_info {
            info.status.mkv_merge.push(merge.status);
        }

        let mkv_split_info = info.get_mkv_split_info();
        info.status.mkv_split.clear();
        for mkv in mkv_split_info {
            info.status.mkv_split.push(mkv.status);
        }

        let mkv_info = info.get_mkv_info();
        info.status.mkv.clear();
        for mkv in mkv_info {
            info.status.mkv.push(mkv.status);
        }

        let mp4_merge_info = info.get_mp4_merge_info();
        info.status.mp4_merge.clear();
        for merge in mp4_merge_info {
            info.status.mp4_merge.push(merge.status);
        }

        let mp4_split_info = info.get_mp4_split_info();
        info.status.mp4_split.clear();
        for mkv in mp4_split_info {
            info.status.mp4_split.push(mkv.status);
        }

        Ok(info)
    }

    pub fn save_status(&self) -> Result<(), VideoToolsError> {
        let json_string = match serde_json::to_string_pretty(&self.status) {
            Ok(s) => s,
            Err(e) => {
                return Err(InfoError(format!(
                    "Failed to convert scan config to String with error: {}",
                    e
                )))
            }
        };

        if !self.folder.exists() {
            return Err(InfoError(format!(
                "Failed to path path does not exit: {}",
                self.folder
            )));
        }

        if let Err(e) = fs::write(self.folder.join("status.json"), json_string) {
            return Err(InfoError(format!(
                "Failed to save Settings to String with error: {}",
                e
            )));
        }

        Ok(())
    }

    pub fn get_iso_info(&self) -> Vec<IsoInfo> {
        let mut info = Vec::new();

        for data in &self.data.iso {
            let data_filename = data.get_filename();
            match self
                .status
                .iso
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(IsoInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(IsoInfo {
                    data: data.clone(),
                    status: IsoStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_iso_status(&mut self, info: &[IsoInfo]) -> Result<(), VideoToolsError> {
        let mut new_iso = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_filename() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Filename `{}` does not match data and status in index {}",
                    item.data.get_filename(),
                    index
                )));
            }

            if !self.data.iso.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing iso data list".to_string(),
                ));
            }

            new_iso.push(item.status.clone())
        }

        self.status.iso = new_iso;

        Ok(())
    }

    pub fn get_mkv_merge_info(&self) -> Vec<MergeInfo> {
        let mut info = Vec::new();

        for data in &self.data.mkv_merge {
            let data_filename = data.get_output_name();
            match self
                .status
                .mkv_merge
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(MergeInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(MergeInfo {
                    data: data.clone(),
                    status: MergeStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_mkv_merge_status(&mut self, info: &[MergeInfo]) -> Result<(), VideoToolsError> {
        let mut new_merge = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_output_name() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Output name `{}` does not match data and status in index {}",
                    item.data.get_output_name(),
                    index
                )));
            }

            if !self.data.mkv_merge.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing mkv_merge data list".to_string(),
                ));
            }

            new_merge.push(item.status.clone())
        }

        self.status.mkv_merge = new_merge;

        Ok(())
    }

    pub fn get_mkv_split_info(&self) -> Vec<SplitInfo> {
        let mut info = Vec::new();

        for data in &self.data.mkv_split {
            let data_filename = data.get_filename();
            match self
                .status
                .mkv_split
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(SplitInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(SplitInfo {
                    data: data.clone(),
                    status: SplitStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_mkv_split_status(&mut self, info: &[SplitInfo]) -> Result<(), VideoToolsError> {
        let mut new_split = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_filename() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Filename `{}` does not match data and status in index {}",
                    item.data.get_filename(),
                    index
                )));
            }

            if !self.data.mkv_split.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing mkv_split data list".to_string(),
                ));
            }

            new_split.push(item.status.clone())
        }

        self.status.mkv_split = new_split;

        Ok(())
    }

    pub fn get_mkv_info(&self) -> Vec<MkvInfo> {
        let mut info = Vec::new();

        for data in &self.data.mkv {
            let data_filename = data.get_filename();
            match self
                .status
                .mkv
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(MkvInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(MkvInfo {
                    data: data.clone(),
                    status: MkvStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_mkv_status(&mut self, info: &[MkvInfo]) -> Result<(), VideoToolsError> {
        let mut new_mkv = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_filename() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Filename `{}` does not match data and status in index {}",
                    item.data.get_filename(),
                    index
                )));
            }

            if !self.data.mkv.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing mkv data list".to_string(),
                ));
            }

            new_mkv.push(item.status.clone())
        }

        self.status.mkv = new_mkv;

        Ok(())
    }

    pub fn get_mp4_merge_info(&self) -> Vec<MergeInfo> {
        let mut info = Vec::new();

        for data in &self.data.mp4_merge {
            let data_filename = data.get_output_name();
            match self
                .status
                .mp4_merge
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(MergeInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(MergeInfo {
                    data: data.clone(),
                    status: MergeStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_mp4_merge_status(&mut self, info: &[MergeInfo]) -> Result<(), VideoToolsError> {
        let mut new_merge = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_output_name() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Output name `{}` does not match data and status in index {}",
                    item.data.get_output_name(),
                    index
                )));
            }

            if !self.data.mp4_merge.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing mp4_merge data list".to_string(),
                ));
            }

            new_merge.push(item.status.clone())
        }

        self.status.mp4_merge = new_merge;

        Ok(())
    }

    pub fn get_mp4_split_info(&self) -> Vec<SplitInfo> {
        let mut info = Vec::new();

        for data in &self.data.mp4_split {
            let data_filename = data.get_filename();
            match self
                .status
                .mp4_split
                .iter()
                .find(|status| status.get_filename() == data_filename)
            {
                Some(s) => info.push(SplitInfo {
                    data: data.clone(),
                    status: s.clone(),
                }),
                None => info.push(SplitInfo {
                    data: data.clone(),
                    status: SplitStatus::new(data_filename),
                }),
            }
        }

        info
    }

    pub fn set_mp4_split_status(&mut self, info: &[SplitInfo]) -> Result<(), VideoToolsError> {
        let mut new_split = Vec::new();

        for (index, item) in info.iter().enumerate() {
            if item.data.get_filename() != item.status.get_filename() {
                return Err(InfoError(format!(
                    "Filename `{}` does not match data and status in index {}",
                    item.data.get_filename(),
                    index
                )));
            }

            if !self.data.mp4_split.iter().any(|i| i == &item.data) {
                return Err(InfoError(
                    "Failed to find data in existing mp4_split data list".to_string(),
                ));
            }

            new_split.push(item.status.clone())
        }

        self.status.mp4_split = new_split;

        Ok(())
    }

    pub fn get_mp4_data(&self) -> Option<Mp4Data> {
        self.data.mp4.clone()
    }
}
