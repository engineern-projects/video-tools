use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct IsoData {
    filename: String,
    check_only: Option<bool>,
    custom_mkv_folder: Option<String>,
}

impl IsoData {
    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }

    pub fn get_check_only(&self) -> Option<bool> {
        self.check_only
    }

    pub fn get_custom_mkv_folder(&self) -> Option<String> {
        self.custom_mkv_folder.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub enum Resolution {
    #[default]
    #[serde(rename = "r480p")]
    R480p,
    #[serde(rename = "mobile")]
    Mobile,
    #[serde(rename = "r720p")]
    R720p,
    #[serde(rename = "tv")]
    Tv,
    #[serde(rename = "r1080p")]
    R1080p,
    #[serde(rename = "r2160p")]
    R2160p,
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct MkvData {
    filename: String,
    resolution: Resolution,
    plex_name: String,
    source: Option<String>,
}

impl MkvData {
    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }

    pub fn get_resolution(&self) -> Resolution {
        self.resolution.clone()
    }

    pub fn get_plex_name(&self) -> String {
        self.plex_name.clone()
    }

    pub fn get_source(&self) -> Option<String> {
        self.source.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct MergeItem {
    filename: String,
    source: Option<String>,
}

impl MergeItem {
    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }

    pub fn get_source(&self) -> Option<String> {
        self.source.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct MergeData {
    filenames: Vec<MergeItem>,
    output_name: String,
}

impl MergeData {
    pub fn get_filenames(&self) -> Vec<MergeItem> {
        self.filenames.clone()
    }

    pub fn get_output_name(&self) -> String {
        self.output_name.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct SplitData {
    filename: String,
    source: Option<String>,
    chapters: Option<String>,
    timestamps: Option<String>,
}

impl SplitData {
    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }

    pub fn get_source(&self) -> Option<String> {
        self.source.clone()
    }

    pub fn get_chapters(&self) -> Option<String> {
        self.chapters.clone()
    }

    pub fn get_timestamps(&self) -> Option<String> {
        self.timestamps.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Mp4Item {
    pattern: String,

    #[serde(default)]
    exclusions: Vec<String>,

    #[serde(rename = "type")]
    type_id: String,
    season: Option<i32>,
    folder_name: String,
}

impl Mp4Item {
    pub fn get_pattern(&self) -> String {
        self.pattern.clone()
    }

    pub fn get_exclusion(&self) -> Vec<String> {
        self.exclusions.clone()
    }

    pub fn get_type_id(&self) -> String {
        self.type_id.clone()
    }

    pub fn get_season(&self) -> Option<i32> {
        self.season
    }

    pub fn get_folder_name(&self) -> String {
        self.folder_name.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Mp4Corrections {
    pattern: Option<String>,
    replacement: String,
    regex: Option<String>,
}

impl Mp4Corrections {
    pub fn get_pattern(&self) -> Option<String> {
        self.pattern.clone()
    }

    pub fn get_replacement(&self) -> String {
        self.replacement.clone()
    }

    pub fn get_regex(&self) -> Option<String> {
        self.regex.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Mp4Data {
    items: Vec<Mp4Item>,

    #[serde(default)]
    corrections: Vec<Mp4Corrections>,
}

impl Mp4Data {
    pub fn get_items(&self) -> Vec<Mp4Item> {
        self.items.clone()
    }

    pub fn get_corrections(&self) -> Vec<Mp4Corrections> {
        self.corrections.clone()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Data {
    pub name: String,

    #[serde(default)]
    pub iso: Vec<IsoData>,

    #[serde(default)]
    pub mkv_split: Vec<SplitData>,

    #[serde(default)]
    pub mkv_merge: Vec<MergeData>,

    #[serde(default)]
    pub mkv: Vec<MkvData>,

    #[serde(default)]
    pub mp4_split: Vec<SplitData>,

    #[serde(default)]
    pub mp4_merge: Vec<MergeData>,

    pub mp4: Option<Mp4Data>,
}
