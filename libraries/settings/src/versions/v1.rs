use camino::Utf8PathBuf;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Settings {
    pub handbrake: Utf8PathBuf,
    pub makemkv: Utf8PathBuf,
    pub mkvinfo: Utf8PathBuf,
    pub mkvmerge: Utf8PathBuf,
    pub output: Utf8PathBuf,
    pub subtitle_edit: Utf8PathBuf,
    pub scan: Utf8PathBuf,
    pub temp: Utf8PathBuf,
}

impl fmt::Display for Settings {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            serde_json::to_string_pretty(&self).unwrap_or_default()
        )
    }
}
