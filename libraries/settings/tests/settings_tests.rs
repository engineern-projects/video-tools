#[macro_use]
extern crate lazy_static;

#[cfg(test)]
use pretty_assertions::{assert_eq, assert_ne};

use camino::Utf8PathBuf;
use errors::VideoToolsError::SettingsError;
use settings::versioned_settings::VersionedSettings;
use settings::SettingsHandler;
use std::{env, fs};

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files");
}

fn remove_file_if_exists(path: Utf8PathBuf) {
    if path.exists() {
        fs::remove_file(path).unwrap()
    }
}

#[test]
fn test_default() {
    let settings_handler = SettingsHandler::new(FILES_DIR.join("default.json")).unwrap();
    assert_eq!(
        settings_handler.get_settings(),
        VersionedSettings::default().get_settings()
    )
}

#[test]
fn test_display() {
    let settings_handler = SettingsHandler::new(FILES_DIR.join("default.json")).unwrap();
    assert_eq!(
        format!("{}", settings_handler.get_settings()), 
        "{\n  \"handbrake\": \"\",\n  \"makemkv\": \"\",\n  \"mkvinfo\": \"\",\n  \"mkvmerge\": \"\",\n  \"output\": \"\",\n  \"subtitle_edit\": \"\",\n  \"scan\": \"\",\n  \"temp\": \"\"\n}"
    )
}

#[test]
fn test_path() {
    let settings_handler = SettingsHandler::new(FILES_DIR.join("default.json")).unwrap();
    assert_eq!(settings_handler.get_path(), FILES_DIR.join("default.json"))
}

#[test]
fn test_save() {
    let path = FILES_DIR.join("save.json");
    let _cleanup = scopeguard::guard(path.clone(), remove_file_if_exists);

    let mut settings_handler = SettingsHandler::new(path.clone()).unwrap();
    {
        let mut settings = settings_handler.get_mut_settings();
        settings.handbrake = path.clone();
    }

    settings_handler.save().unwrap();

    let settings = settings_handler.get_settings();
    let settings_handler_load = SettingsHandler::new(path).unwrap();
    assert_eq!(settings, settings_handler_load.get_settings());
    assert_ne!(settings, VersionedSettings::default().get_settings());
}

#[test]
fn test_missing_version() {
    let path = FILES_DIR.join("missing_version.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `version` at line 10 column 1",
            path
        )))
    );
}

#[test]
fn test_missing_handbrake() {
    let path = FILES_DIR.join("missing_handbrake.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `handbrake`",
            path
        )))
    );
}

#[test]
fn test_missing_mkvinfo() {
    let path = FILES_DIR.join("missing_mkvinfo.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `mkvinfo`",
            path
        )))
    );
}

#[test]
fn test_missing_mkvmerge() {
    let path = FILES_DIR.join("missing_mkvmerge.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `mkvmerge`",
            path
        )))
    );
}

#[test]
fn test_missing_output() {
    let path = FILES_DIR.join("missing_output.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `output`",
            path
        )))
    );
}

#[test]
fn test_missing_subtitle_edit() {
    let path = FILES_DIR.join("missing_subtitle_edit.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `subtitle_edit`",
            path
        )))
    );
}

#[test]
fn test_missing_scan() {
    let path = FILES_DIR.join("missing_scan.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `scan`",
            path
        )))
    );
}

#[test]
fn test_missing_temp() {
    let path = FILES_DIR.join("missing_temp.json");
    assert_eq!(
        SettingsHandler::new(path.clone()).err(),
        Some(SettingsError(format!(
            "Failed to read settings `{}` with error: missing field `temp`",
            path
        )))
    );
}
