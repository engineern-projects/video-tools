#[cfg(test)]
use pretty_assertions::assert_eq;

use camino::Utf8PathBuf;
use errors::VideoToolsError;
use lazy_static::lazy_static;
use scan_config::ScanConfigHandler;
use std::{env, fs};
use video_tools::builder::generate_command;
use video_tools::command::Command;
use video_tools::commands::scan_config::ScanConfigCommand;

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files")
        .join("scan_config");
    static ref DEFAULT_DIR: Utf8PathBuf = FILES_DIR.join("default");
    static ref DEFAULT_SCAN_CONFIG_PATH: Utf8PathBuf = DEFAULT_DIR.join("scan_config.json");
    static ref EXISTING_DIR: Utf8PathBuf = FILES_DIR.join("existing");
    static ref EXISTING_SCAN_CONFIG_PATH: Utf8PathBuf = EXISTING_DIR.join("scan_config.json");
}

fn check_scan_config_command(
    file: Utf8PathBuf,
    init_errors: Vec<VideoToolsError>,
    execute_errors: Vec<VideoToolsError>,
) {
    let scan_config_handler = ScanConfigHandler::new(file).unwrap();
    let mut command = ScanConfigCommand::default();
    command.set_scan_config(scan_config_handler);
    command.initialize();
    if !init_errors.is_empty() {
        assert_eq!(command.get_errors(), init_errors);
        return;
    } else {
        assert_eq!(command.get_errors(), Vec::new());
    }
    command.execute();
    assert_eq!(command.get_errors(), execute_errors);
}

fn check_scan_config(path: Utf8PathBuf) {
    let scan_config_handler = ScanConfigHandler::new(path.clone()).unwrap();
    let mut default_scan_config_handler = ScanConfigHandler::default();
    default_scan_config_handler.set_path(path);

    assert_eq!(scan_config_handler, default_scan_config_handler);
}

fn remove_file_if_exists(path: Utf8PathBuf) {
    if path.exists() {
        fs::remove_file(path).unwrap()
    }
}

#[test]
fn test_empty_arguments() {
    let arguments = vec!["video_tools", "scan_config"];

    assert_eq!(
        generate_command().try_get_matches_from(arguments).is_ok(),
        true
    );
}

#[test]
fn test_default() {
    let default_scan_config = DEFAULT_SCAN_CONFIG_PATH.clone();
    let _cleanup = scopeguard::guard(default_scan_config.clone(), remove_file_if_exists);
    remove_file_if_exists(default_scan_config.clone());

    assert_eq!(default_scan_config.exists(), false);
    check_scan_config_command(DEFAULT_SCAN_CONFIG_PATH.clone(), Vec::new(), Vec::new());
    assert_eq!(default_scan_config.exists(), true);
}

#[test]
fn test_existing() {
    let existing_scan_config = EXISTING_SCAN_CONFIG_PATH.clone();

    assert_eq!(existing_scan_config.exists(), true);
    check_scan_config(existing_scan_config.clone());

    check_scan_config_command(EXISTING_SCAN_CONFIG_PATH.clone(), Vec::new(), Vec::new());

    assert_eq!(existing_scan_config.exists(), true);
    check_scan_config(existing_scan_config);
}
