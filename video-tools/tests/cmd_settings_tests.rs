#[cfg(test)]
use pretty_assertions::assert_eq;

use camino::Utf8PathBuf;
use clap::error::ErrorKind;
use clap::ArgMatches;
use errors::VideoToolsError;
use errors::VideoToolsError::GeneralError;
use lazy_static::lazy_static;
use settings::SettingsHandler;
use std::{env, fs};
use video_tools::builder::generate_command;
use video_tools::command::Command;
use video_tools::commands::settings::SettingsCommand;

fn generate_arguments(args: Vec<&str>) -> ArgMatches {
    let mut arguments = vec!["video_tools", "settings"];
    arguments.extend(args);

    match generate_command()
        .try_get_matches_from(arguments)
        .unwrap()
        .subcommand()
    {
        Some(("settings", matches)) => matches.to_owned(),
        _ => unreachable!("parser should ensure only valid subcommand names are used"),
    }
}

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files")
        .join("settings");
    static ref DUMMY: Utf8PathBuf = FILES_DIR.join("dummy");
    static ref INVALID: Utf8PathBuf = FILES_DIR.join("invalid");
    static ref DEFAULT: Utf8PathBuf = FILES_DIR.join("default.json");
}

fn check_settings(file: Utf8PathBuf, argument_matches: ArgMatches, errors: Vec<VideoToolsError>) {
    let settings_handler = SettingsHandler::new(file).unwrap();
    let mut command = SettingsCommand::default();
    command.set_settings(settings_handler);
    command.set_argument_matches(argument_matches);
    command.initialize();
    command.execute();
    assert_eq!(command.get_errors(), errors);
}

fn remove_file_if_exists(path: Utf8PathBuf) {
    if path.exists() {
        fs::remove_file(path).unwrap()
    }
}

#[test]
fn test_empty_arguments() {
    let arguments = vec!["video_tools", "settings"];

    assert_eq!(
        generate_command()
            .try_get_matches_from(arguments)
            .unwrap_err()
            .kind(),
        ErrorKind::DisplayHelpOnMissingArgumentOrSubcommand
    );
}

#[test]
fn test_dry_run() {
    let args = Vec::from(["--dry_run"]);

    check_settings(DEFAULT.clone(), generate_arguments(args), Vec::new())
}

#[test]
fn test_update_handbrake() {
    let temp_file = FILES_DIR.join("temp_handbrake.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--handbrake", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.handbrake, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_makemkv() {
    let temp_file = FILES_DIR.join("temp_makemkv.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--makemkv", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.makemkv, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_mkvinfo() {
    let temp_file = FILES_DIR.join("temp_mkvinfo.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--mkvinfo", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.mkvinfo, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_mkvmerge() {
    let temp_file = FILES_DIR.join("temp_mkvmerge.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--mkvmerge", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.mkvmerge, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_output() {
    let temp_file = FILES_DIR.join("temp_output.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--output", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.output, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_subtitle_edit() {
    let temp_file = FILES_DIR.join("temp_subtitle_edit.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--subtitle_edit", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.subtitle_edit, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.scan, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_scan() {
    let temp_file = FILES_DIR.join("temp_scan.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--scan", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.scan, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.temp, empty_path);
    }
}

#[test]
fn test_update_temp() {
    let temp_file = FILES_DIR.join("temp_temp.json");
    let _cleanup = scopeguard::guard(temp_file.clone(), remove_file_if_exists);

    let args = Vec::from(["--temp", DUMMY.as_str()]);

    check_settings(temp_file.clone(), generate_arguments(args), Vec::new());
    let settings_handler = SettingsHandler::new(temp_file).unwrap();
    {
        let settings = settings_handler.get_settings();
        assert_eq!(settings.temp, DUMMY.clone());

        let empty_path = Utf8PathBuf::new();
        assert_eq!(settings.handbrake, empty_path);
        assert_eq!(settings.makemkv, empty_path);
        assert_eq!(settings.mkvinfo, empty_path);
        assert_eq!(settings.mkvmerge, empty_path);
        assert_eq!(settings.output, empty_path);
        assert_eq!(settings.subtitle_edit, empty_path);
        assert_eq!(settings.scan, empty_path);
    }
}

fn check_invalid(id: &str) {
    let arg = format!("--{}", id);
    let args = Vec::from(["--dry_run", arg.as_str(), INVALID.as_str()]);
    let errors = vec![GeneralError(format!(
        "Failed to find given path for {}: {}",
        id,
        INVALID.as_str()
    ))];

    check_settings(DEFAULT.clone(), generate_arguments(args), errors);
}

#[test]
fn test_invalid_handbrake() {
    check_invalid("handbrake");
}

#[test]
fn test_invalid_makemkv() {
    check_invalid("makemkv");
}

#[test]
fn test_invalid_mkvinfo() {
    check_invalid("mkvinfo");
}

#[test]
fn test_invalid_mkvmerge() {
    check_invalid("mkvmerge");
}

#[test]
fn test_invalid_output() {
    check_invalid("output");
}

#[test]
fn test_invalid_subtitle_edit() {
    check_invalid("subtitle_edit");
}

#[test]
fn test_invalid_scan() {
    check_invalid("scan");
}

#[test]
fn test_invalid_temp() {
    check_invalid("temp");
}
