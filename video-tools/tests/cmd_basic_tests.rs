#[cfg(test)]
use pretty_assertions::assert_eq;

use clap::error::ErrorKind;
use video_tools::builder::generate_command;

#[test]
fn test_empty_arguments() {
    let arguments = vec!["video_tools"];

    assert_eq!(
        generate_command()
            .try_get_matches_from(arguments)
            .unwrap_err()
            .kind(),
        ErrorKind::DisplayHelpOnMissingArgumentOrSubcommand
    );
}
