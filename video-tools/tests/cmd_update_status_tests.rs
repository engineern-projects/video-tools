#[cfg(test)]
use pretty_assertions::{assert_eq, assert_ne};

use camino::Utf8PathBuf;
use errors::VideoToolsError;
use info::Info;
use lazy_static::lazy_static;
use scan_config::ScanConfigHandler;
use std::{env, fs};
use video_tools::builder::generate_command;
use video_tools::command::Command;
use video_tools::commands::update_status::UpdateStatusCommand;

lazy_static! {
    static ref FILES_DIR: Utf8PathBuf = Utf8PathBuf::from_path_buf(env::current_dir().unwrap())
        .unwrap()
        .join("tests")
        .join("files")
        .join("update_status");
    static ref CREATION_DIR: Utf8PathBuf = FILES_DIR.join("creation");
    static ref CREATION_SCAN_CONFIG_PATH: Utf8PathBuf = CREATION_DIR.join("scan_config.json");
    static ref EXISTING_DIR: Utf8PathBuf = FILES_DIR.join("existing");
    static ref EXISTING_SCAN_CONFIG_PATH: Utf8PathBuf = EXISTING_DIR.join("scan_config.json");
    static ref UPDATE_DIR: Utf8PathBuf = FILES_DIR.join("update");
    static ref UPDATE_SCAN_CONFIG_PATH: Utf8PathBuf = UPDATE_DIR.join("scan_config.json");
}

fn check_update_status(
    file: Utf8PathBuf,
    init_errors: Vec<VideoToolsError>,
    execute_errors: Vec<VideoToolsError>,
) {
    let scan_config_handler = ScanConfigHandler::new(file).unwrap();
    let mut command = UpdateStatusCommand::default();
    command.set_scan_config(scan_config_handler);
    command.initialize();
    if !init_errors.is_empty() {
        assert_eq!(command.get_errors(), init_errors);
        return;
    } else {
        assert_eq!(command.get_errors(), Vec::new());
    }
    command.execute();
    assert_eq!(command.get_errors(), execute_errors);
}

fn check_info(folder: &Utf8PathBuf) {
    let info = Info::new(folder).unwrap();
    assert_eq!(info.get_iso_info(), Vec::new());
    assert_eq!(info.get_mkv_info(), Vec::new());
    assert_eq!(info.get_mkv_split_info(), Vec::new());
    assert_eq!(info.get_mkv_merge_info(), Vec::new());
    assert_eq!(info.get_mp4_data(), None);
    assert_eq!(info.get_mp4_split_info(), Vec::new());
    assert_eq!(info.get_mp4_merge_info(), Vec::new());
}

fn remove_file_if_exists(path: Utf8PathBuf) {
    if path.exists() {
        fs::remove_file(path).unwrap()
    }
}

#[test]
fn test_empty_arguments() {
    let arguments = vec!["video_tools", "update_status"];

    assert_eq!(
        generate_command().try_get_matches_from(arguments).is_ok(),
        true
    );
}

#[test]
fn test_status_creation() {
    let movie_folder = CREATION_DIR.join("Movies").join("A Movie");
    let movie_status_file = movie_folder.join("status.json");
    let _cleanup = scopeguard::guard(movie_status_file.clone(), remove_file_if_exists);

    let tv_folder = CREATION_DIR
        .join("TV Shows")
        .join("A TV Show")
        .join("Season 01");
    let tv_status_file = tv_folder.join("status.json");
    let _cleanup = scopeguard::guard(tv_status_file.clone(), remove_file_if_exists);

    check_update_status(CREATION_SCAN_CONFIG_PATH.clone(), Vec::new(), Vec::new());

    assert_eq!(movie_status_file.exists(), true);
    assert_eq!(tv_status_file.exists(), true);

    check_info(&movie_folder);
    check_info(&tv_folder);
}

#[test]
fn test_status_existing() {
    let movie_folder = EXISTING_DIR.join("Movies").join("A Movie");
    let movie_status_file = movie_folder.join("status.json");

    let tv_folder = EXISTING_DIR
        .join("TV Shows")
        .join("A TV Show")
        .join("Season 01");
    let tv_status_file = tv_folder.join("status.json");

    assert_eq!(movie_status_file.exists(), true);
    assert_eq!(tv_status_file.exists(), true);

    check_info(&movie_folder);
    check_info(&tv_folder);

    check_update_status(EXISTING_SCAN_CONFIG_PATH.clone(), Vec::new(), Vec::new());

    assert_eq!(movie_status_file.exists(), true);
    assert_eq!(tv_status_file.exists(), true);

    check_info(&movie_folder);
    check_info(&tv_folder);
}

#[test]
fn test_status_update() {
    let movie_folder = UPDATE_DIR.join("Movies").join("A Movie");
    let movie_original_status_file = movie_folder.join("original_status.json");
    let movie_status_file = movie_folder.join("status.json");
    let _cleanup = scopeguard::guard(movie_status_file.clone(), remove_file_if_exists);
    remove_file_if_exists(movie_status_file.clone());
    fs::copy(
        movie_original_status_file.clone(),
        movie_status_file.clone(),
    )
    .unwrap();

    let tv_folder = UPDATE_DIR
        .join("TV Shows")
        .join("A TV Show")
        .join("Season 01");
    let tv_original_status_file = tv_folder.join("original_status.json");
    let tv_status_file = tv_folder.join("status.json");
    let _cleanup = scopeguard::guard(tv_status_file.clone(), remove_file_if_exists);
    remove_file_if_exists(tv_status_file.clone());
    fs::copy(tv_original_status_file.clone(), tv_status_file.clone()).unwrap();

    assert_eq!(movie_status_file.exists(), true);
    assert_eq!(tv_status_file.exists(), true);

    check_info(&movie_folder);
    check_info(&tv_folder);

    check_update_status(UPDATE_SCAN_CONFIG_PATH.clone(), Vec::new(), Vec::new());

    assert_eq!(movie_status_file.exists(), true);
    assert_eq!(tv_status_file.exists(), true);

    check_info(&movie_folder);
    check_info(&tv_folder);

    assert_ne!(
        fs::read_to_string(movie_original_status_file).unwrap(),
        fs::read_to_string(movie_status_file).unwrap()
    );
    assert_ne!(
        fs::read_to_string(tv_original_status_file).unwrap(),
        fs::read_to_string(tv_status_file).unwrap()
    );
}
