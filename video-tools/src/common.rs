use any_ascii::any_ascii;
use console::Style;
use indicatif::{ProgressBar, ProgressStyle};
use lazy_static::lazy_static;
use std::ops::Deref;

lazy_static! {
    static ref CYAN_TITLE: Style = Style::new().bold().cyan();
    static ref RED_TITLE: Style = Style::new().bold().red();
    static ref WHITE: Style = Style::new().white();
}

fn print_common(display: &str, style: &Style) {
    println!("{}", style.apply_to(display));
}

pub fn print_cyan_title<S: AsRef<str>>(display: S) {
    println!();
    print_common(display.as_ref(), CYAN_TITLE.deref());
}

pub fn print_red_title<S: AsRef<str>>(display: S) {
    println!();
    print_common(display.as_ref(), RED_TITLE.deref());
}

pub fn print_white<S: AsRef<str>>(display: S) {
    print_common(display.as_ref(), WHITE.deref());
}

pub fn create_progress_bar(count: u64) -> ProgressBar {
    let pb = ProgressBar::new(count);
    pb.set_style(
        ProgressStyle::with_template(
            "{spinner:.cyan} [{elapsed_precise}] {bar:40.cyan/blue} {pos:>7}/{len:7} {msg:50}",
        )
        .unwrap()
        .progress_chars("#>-"),
    );
    pb
}

pub trait CleanProgressBar {
    fn clean_set_message(&self, msg: &str);
}

impl CleanProgressBar for ProgressBar {
    fn clean_set_message(&self, msg: &str) {
        self.set_message(any_ascii(msg));
    }
}
