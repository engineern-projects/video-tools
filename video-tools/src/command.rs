use crate::common::{print_red_title, print_white};
use camino::Utf8PathBuf;
use clap::ArgMatches;
use errors::VideoToolsError;
use errors::VideoToolsError::GeneralError;
use scan_config::ScanConfigHandler;
use settings::SettingsHandler;

fn get_config_file_path() -> Result<Utf8PathBuf, VideoToolsError> {
    let config = dirs::config_dir();

    match config {
        Some(path) => {
            match Utf8PathBuf::try_from(path) {
                Ok(utf8_path) => {
                    Ok(utf8_path.join(concat!("dev.engineern.", env!("CARGO_PKG_NAME"))))
                }

                // This should never happen...
                _ => Err(GeneralError(
                    "Failed to convert config path to UTF8.".into(),
                )),
            }
        }

        // This should never happen...
        _ => Err(GeneralError("Failed to find config path.".into())),
    }
}

fn get_settings_file_path() -> Result<Utf8PathBuf, VideoToolsError> {
    match get_config_file_path() {
        Ok(path) => Ok(path.join("settings.json")),
        Err(e) => Err(e),
    }
}

fn get_settings() -> Result<SettingsHandler, VideoToolsError> {
    SettingsHandler::new(get_settings_file_path()?)
}

fn get_scan_config(scan_path: Utf8PathBuf) -> Result<ScanConfigHandler, VideoToolsError> {
    let scan_config_path = scan_path.join("scan_config.json");

    ScanConfigHandler::new(scan_config_path)
}

pub trait Command {
    fn need_settings(&self) -> bool {
        false
    }
    fn set_settings(&mut self, settings: SettingsHandler);
    fn need_scan_config(&self) -> bool {
        false
    }
    fn set_scan_config(&mut self, scan_config: ScanConfigHandler);
    fn need_argument_matches(&self) -> bool {
        false
    }
    fn set_argument_matches(&mut self, arguments: ArgMatches);
    fn initialize(&mut self);
    fn execute(&mut self);
    fn get_errors(&self) -> Vec<VideoToolsError>;
}

pub fn command_handler(mut command: impl Command, argument_matches: ArgMatches) {
    let settings_handler = match get_settings() {
        Ok(handler) => handler,
        Err(e) => {
            println!("{}", e);
            return;
        }
    };

    if command.need_settings() {
        command.set_settings(settings_handler.clone())
    }

    if command.need_scan_config() {
        let scan_config_handler =
            match get_scan_config(settings_handler.get_settings().scan.clone()) {
                Ok(handler) => handler,
                Err(e) => {
                    println!("{}", e);
                    return;
                }
            };
        command.set_scan_config(scan_config_handler)
    }

    if command.need_argument_matches() {
        command.set_argument_matches(argument_matches);
    }

    command.initialize();
    let errors = command.get_errors();
    if !errors.is_empty() {
        print_red_title("The following errors occurred:");
        for error in errors {
            print_white(format!("- {}", error));
        }
        return;
    }

    command.execute();
    let errors = command.get_errors();
    if !errors.is_empty() {
        print_red_title("The following errors occurred:");
        for error in errors {
            print_white(format!("- {}", error));
        }
    }
}
