use video_tools::builder::generate_command;
use video_tools::command::command_handler;
use video_tools::commands::scan_config::ScanConfigCommand;
use video_tools::commands::settings::SettingsCommand;
use video_tools::commands::update_status::UpdateStatusCommand;

fn main() {
    let cmd = generate_command();

    match cmd.get_matches().subcommand() {
        Some(("check_iso_hash", _matches)) => {
            println!("check_iso_hash");
        }
        Some(("file_organizer", _matches)) => {
            println!("file_organizer");
        }
        Some(("iso_to_mkv", _matches)) => {
            println!("iso_to_mkv");
        }
        Some(("mkv_cleanup", _matches)) => {
            println!("mkv_cleanup");
        }
        Some(("mkv_merge", _matches)) => {
            println!("mkv_merge");
        }
        Some(("mkv_split", _matches)) => {
            println!("mkv_split");
        }
        Some(("mkv_to_mp4", _matches)) => {
            println!("mkv_to_mp4");
        }
        Some(("update_status", matches)) => {
            command_handler(UpdateStatusCommand::default(), matches.to_owned())
        }
        Some(("scan_config", matches)) => {
            command_handler(ScanConfigCommand::default(), matches.to_owned())
        }
        Some(("settings", matches)) => {
            command_handler(SettingsCommand::default(), matches.to_owned())
        }
        _ => unreachable!("parser should ensure only valid subcommand names are used"),
    };
}
