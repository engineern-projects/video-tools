use camino::Utf8PathBuf;
use clap::{arg, value_parser, Command};

pub fn generate_command() -> Command {
    Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .arg_required_else_help(true)
        .subcommand(Command::new("check_iso_hash").about("Check the md5 hash of the ISO files."))
        .subcommand(
            Command::new("file_organizer").about("Organize the files from the backup directory."),
        )
        .subcommand(Command::new("iso_to_mkv").about("Convert ISO files to MKV files."))
        .subcommand(Command::new("mkv_cleanup").about("Cleanup extra MKV files."))
        .subcommand(Command::new("mkv_merge").about("Merge MKV files."))
        .subcommand(Command::new("mkv_split").about("Split MKV files."))
        .subcommand(Command::new("mkv_to_mp4").about("Convert MKV files to MP4."))
        .subcommand(Command::new("scan_config").about("Creates a default scan_config.json file if one does not exist and prints the content of the scan_config.json file."))
        .subcommand(Command::new("update_status").about("Update the status files."))
        .subcommand(
            Command::new("settings")
                .arg_required_else_help(true)
                .about("Updates and prints the settings.")
                .arg(
                    arg!(--handbrake <PATH> "New path to be used for handbrake.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--makemkv <PATH> "New path to be used for makemkv.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--mkvinfo <PATH> "New path to be used for mkvinfo.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--mkvmerge <PATH> "New path to be used for mkvmerge.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--output <PATH> "New directory path to be used for output.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--subtitle_edit <PATH> "New path to be used for subtitle_edit.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--scan <PATH> "New directory path to be used for scanning.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--temp <PATH> "New directory path to be used for temporary data.")
                        .value_parser(value_parser!(Utf8PathBuf))
                        .required(false),
                )
                .arg(
                    arg!(--dry_run "Simulate update to settings without saving changes.")
                        .required(false),
                ),
        )
}
