use crate::command::Command;
use crate::common::{print_cyan_title, print_white};
use camino::Utf8PathBuf;
use clap::ArgMatches;
use errors::VideoToolsError;
use errors::VideoToolsError::GeneralError;
use scan_config::ScanConfigHandler;
use settings::SettingsHandler;

#[derive(Default)]
pub struct SettingsCommand {
    settings: SettingsHandler,
    argument_matches: ArgMatches,
    errors: Vec<VideoToolsError>,
}

impl Command for SettingsCommand {
    fn need_settings(&self) -> bool {
        true
    }

    fn set_settings(&mut self, settings: SettingsHandler) {
        self.settings = settings;
    }

    fn set_scan_config(&mut self, _: ScanConfigHandler) {}

    fn need_argument_matches(&self) -> bool {
        true
    }

    fn set_argument_matches(&mut self, argument_matches: ArgMatches) {
        self.argument_matches = argument_matches;
    }

    fn initialize(&mut self) {
        print_cyan_title("Current path for settings file:");
        print_white(self.settings.get_path());
    }

    fn execute(&mut self) {
        let settings = self.settings.get_mut_settings();

        // Print current settings.
        print_cyan_title("Current settings data:");
        print_white(format!("{}", settings));

        let mut has_changed = false;

        let mut update_item = |id: &str, item: &mut Utf8PathBuf| {
            if let Some(argument_path) = self.argument_matches.get_one::<Utf8PathBuf>(id) {
                if argument_path.exists() {
                    item.clone_from(argument_path);
                    has_changed = true;
                } else {
                    self.errors.push(GeneralError(format!(
                        "Failed to find given path for {}: {}",
                        id, argument_path
                    )));
                }
            }
        };

        update_item("handbrake", &mut settings.handbrake);
        update_item("makemkv", &mut settings.makemkv);
        update_item("mkvinfo", &mut settings.mkvinfo);
        update_item("mkvmerge", &mut settings.mkvmerge);
        update_item("output", &mut settings.output);
        update_item("subtitle_edit", &mut settings.subtitle_edit);
        update_item("scan", &mut settings.scan);
        update_item("temp", &mut settings.temp);

        // Print updated settings if it has changed.
        if has_changed {
            print_cyan_title("Updated settings data:");
            print_white(format!("{}", settings));
        }

        // Save updated settings.
        if !self.argument_matches.get_flag("dry_run") {
            if let Err(e) = self.settings.save() {
                self.errors.push(e);
            }
        }
    }

    fn get_errors(&self) -> Vec<VideoToolsError> {
        self.errors.clone()
    }
}
