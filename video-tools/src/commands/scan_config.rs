use crate::command::Command;
use crate::common::{print_cyan_title, print_white};
use clap::ArgMatches;
use errors::VideoToolsError;
use scan_config::ScanConfigHandler;
use settings::SettingsHandler;

#[derive(Default)]
pub struct ScanConfigCommand {
    scan_config: ScanConfigHandler,
    errors: Vec<VideoToolsError>,
}

impl Command for ScanConfigCommand {
    fn set_settings(&mut self, _settings: SettingsHandler) {}

    fn need_scan_config(&self) -> bool {
        true
    }

    fn set_scan_config(&mut self, scan_config: ScanConfigHandler) {
        self.scan_config = scan_config;
    }

    fn set_argument_matches(&mut self, _arguments: ArgMatches) {}

    fn initialize(&mut self) {
        print_cyan_title("Current path for scan config file:");
        print_white(self.scan_config.get_path());
    }

    fn execute(&mut self) {
        if !self.scan_config.get_path().exists() {
            if let Err(error) = self.scan_config.save() {
                self.errors.push(error);
                return;
            }
        }

        print_cyan_title("Current scan_config data:");
        print_white(format!("{}", self.scan_config.get_scan_config()));
    }

    fn get_errors(&self) -> Vec<VideoToolsError> {
        self.errors.clone()
    }
}
