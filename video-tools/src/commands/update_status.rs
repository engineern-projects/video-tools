use crate::command::Command;
use crate::common::{create_progress_bar, print_cyan_title, print_white, CleanProgressBar};
use clap::ArgMatches;
use errors::VideoToolsError;
use info_crawler::InfoCrawler;
use scan_config::ScanConfigHandler;
use settings::SettingsHandler;

#[derive(Default)]
pub struct UpdateStatusCommand {
    scan_config: ScanConfigHandler,
    info_crawler: InfoCrawler,
    errors: Vec<VideoToolsError>,
}

impl Command for UpdateStatusCommand {
    fn need_settings(&self) -> bool {
        false
    }

    fn set_settings(&mut self, _settings: SettingsHandler) {}

    fn need_scan_config(&self) -> bool {
        true
    }

    fn set_scan_config(&mut self, scan_config: ScanConfigHandler) {
        self.scan_config = scan_config;
    }

    fn set_argument_matches(&mut self, _argument_matches: ArgMatches) {}

    fn initialize(&mut self) {
        print_cyan_title("Current path for scan config file:");
        print_white(self.scan_config.get_path());

        // Grab the Info.
        print_cyan_title("Grabbing Data/Status files");
        if let Err(errors) = self.info_crawler.get_info(&self.scan_config) {
            self.errors.extend(errors);
        }
    }

    fn execute(&mut self) {
        // Grab the Info.
        print_cyan_title("Updating Status files");
        let pb = create_progress_bar(self.info_crawler.len() as u64);
        for (index, info_item) in self.info_crawler.iter_mut().enumerate() {
            pb.clean_set_message(info_item.get_relative_folder());
            pb.set_position(index as u64);

            if info_item.has_errors() {
                self.errors.extend(info_item.get_errors());
                continue;
            }

            let info = info_item.get_info();
            if let Err(error) = info.save_status() {
                info_item.add_error(error.clone());
                self.errors.push(error);
            }
        }
        pb.finish_and_clear();
    }

    fn get_errors(&self) -> Vec<VideoToolsError> {
        self.errors.clone()
    }
}
